﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        [StringLength(60000, ErrorMessage = "Độ dài phải từ 5 kí tự", MinimumLength = 5)]
        public String Body { set; get; }

        [DataType(DataType.DateTime, ErrorMessage = "Định dạng kiểu datetime")]
        public DateTime DateCreate { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}