﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Độ dài từ 10 đến 500 kí tự", MinimumLength = 2)]
        public String FirstName { set; get; }
        [StringLength(100, ErrorMessage = "Độ dài từ 10 đến 500 kí tự", MinimumLength = 2)]
        public String LastName { set; get; }
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [DataType(DataType.EmailAddress, ErrorMessage = "Nhập đúng địa chỉ Email")]
        public String Email { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}