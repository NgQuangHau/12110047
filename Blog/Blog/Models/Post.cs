﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]

        [StringLength(500, ErrorMessage = "Độ dài từ 10 đến 500 kí tự", MinimumLength = 20)]
        public String Title { set; get; }
        [StringLength(60000, ErrorMessage = "Độ dài phải từ 50 kí tự", MinimumLength = 50)]
        public String Body { set; get; }
        
        [DataType(DataType.DateTime, ErrorMessage = "Định dạng kiểu datetime")]
        public DateTime DateCreate { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

        public int AccID { set; get; }
        public virtual Account Accounts { set; get; }
    }
}