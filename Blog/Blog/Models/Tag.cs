﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Độ dài từ 10 đến 100 kí tự", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}