namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blog4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
        }
    }
}
