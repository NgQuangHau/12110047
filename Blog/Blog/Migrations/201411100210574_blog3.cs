namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blog3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Posts", "AccID", c => c.Int(nullable: false));
            AddColumn("dbo.Posts", "Accounts_AccountID", c => c.Int());
            AddForeignKey("dbo.Posts", "Accounts_AccountID", "dbo.Accounts", "AccountID");
            CreateIndex("dbo.Posts", "Accounts_AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "Accounts_AccountID" });
            DropForeignKey("dbo.Posts", "Accounts_AccountID", "dbo.Accounts");
            DropColumn("dbo.Posts", "Accounts_AccountID");
            DropColumn("dbo.Posts", "AccID");
            DropTable("dbo.Accounts");
        }
    }
}
