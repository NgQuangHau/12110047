﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject_Lifestyle.com.Models
{
    public class GiamCan
    {
        [Key]
        public int BaivietID { set; get; }
        [StringLength(100, ErrorMessage = "Độ dài từ 10 đến 100 kí tự", MinimumLength = 10)]
        public String Tenbaiviet { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime Ngaygio { set; get; }
         [StringLength(100, ErrorMessage = "Độ dài từ 10 đến 100 kí tự", MinimumLength = 10)]
        public String Tomtat { set; get; }
        [StringLength(10, ErrorMessage = "Độ dài từ 10 đến 2000 kí tự", MinimumLength = 10)]
        public String Noidung { set; get; }
        public int Sodiemdanhgia { set; get; }
        public int Solanxem { set; get; }
        public String Hinhanh { set; get; }

        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }

        public virtual ICollection<BinhLuan> Binhluans { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }
    }
}