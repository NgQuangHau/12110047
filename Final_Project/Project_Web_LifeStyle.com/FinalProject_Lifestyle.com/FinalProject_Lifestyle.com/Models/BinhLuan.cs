﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject_Lifestyle.com.Models
{
    public class BinhLuan
    {
        public int BinhluanID { set; get; }
        public String Tieude { set; get; }
        public String Noidung { set; get; }
        public DateTime Ngaygioviet { set; get; }

        public int BaivietID_TDTT { set; get; }
        public virtual TDTT TDTT { set; get; }

        public int BaivietID_TangCan { set; get; }
        public virtual TangCan TangCan { set; get; }

        public int BaiVietID_GiamCan { set; get; }
        public virtual GiamCan GiamCan { set; get; }

    }
}