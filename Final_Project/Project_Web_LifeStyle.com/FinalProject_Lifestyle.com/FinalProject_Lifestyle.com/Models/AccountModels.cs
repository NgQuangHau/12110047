﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace FinalProject_Lifestyle.com.Models
{
    public class LifestyleDbContext : DbContext
    {
        public LifestyleDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<TDTT> TDTTs { get; set; }
        public DbSet<TangCan> TangCans { get; set; }
        public DbSet<GiamCan> GiamCans { get; set; }
        public DbSet<BaiViet> BaiViets { set; get; }
        public DbSet<Thuong> Thuongs { set; get; }
        public DbSet<WebLienKet> WebLienKets { set; get; }
        public DbSet<BinhLuan> BinhLuans { set; get; }
        public DbSet<Tag> Tags { set; get; }
        public DbSet<Congcu> Congcus { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TDTT>().HasMany(t => t.Tags)
                .WithMany(p => p.TDTTs).Map(k => k.MapLeftKey("TDTTID").MapRightKey("TagID").ToTable("Tag_TDTT"));

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TangCan>().HasMany(t => t.Tags)
                .WithMany(p => p.TangCans).Map(k => k.MapLeftKey("TangCanID").MapRightKey("TagID").ToTable("Tag_TangCan"));

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<GiamCan>().HasMany(t => t.Tags)
                .WithMany(p => p.GiamCans).Map(k => k.MapLeftKey("GiamCanID").MapRightKey("TagID").ToTable("Tag_GiamCan"));
        }
        
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }

        public ICollection<BaiViet> BaiViets { set; get; }
        public ICollection<TDTT> TDTTs { set; get; }
        public ICollection<TangCan> TangCans { set; get; }
        public ICollection<GiamCan> GiamCans { set; get; }


    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
