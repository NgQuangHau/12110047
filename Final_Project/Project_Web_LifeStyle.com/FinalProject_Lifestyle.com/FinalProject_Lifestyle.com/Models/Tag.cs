﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject_Lifestyle.com.Models
{
    public class Tag
    {
        public String TagID { get; set; }
        public String Content { set; get; }

        public virtual ICollection<TDTT> TDTTs { set; get; }
        public virtual ICollection<TangCan> TangCans { set; get; }
        public virtual ICollection<GiamCan> GiamCans { set; get; }


    }
}