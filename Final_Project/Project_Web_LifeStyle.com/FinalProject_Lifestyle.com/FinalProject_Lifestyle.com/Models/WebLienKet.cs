﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject_Lifestyle.com.Models
{
    public class WebLienKet
    {
        public int WeblienketID { set; get; }
        public String Tenweb { set; get; }
        public String Link { set; get; }
    }
}