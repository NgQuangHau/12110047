﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject_Lifestyle.com.Models
{
    public class BaiViet
    {
        public int BaivietID { set; get; }
        public String Tenbaiviet { set; get; }
        public DateTime Ngaygio { set; get; }
        public String Tomtat { set; get; }
        public String Noidung { set; get; }
        public int Sodiemdanhgia { set; get; }
        public int Solanxem { set; get; }
        public String Hinhanh { set; get; }

        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }

        
    }
}