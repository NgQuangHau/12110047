namespace FinalProject_Lifestyle.com.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.BaiViets",
                c => new
                    {
                        BaivietID = c.Int(nullable: false, identity: true),
                        Tenbaiviet = c.String(),
                        Ngaygio = c.DateTime(nullable: false),
                        Tomtat = c.String(),
                        Noidung = c.String(),
                        Sodiemdanhgia = c.Int(nullable: false),
                        Solanxem = c.Int(nullable: false),
                        Hinhanh = c.String(),
                        DanhMucID = c.Int(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                        BaiViet_BaivietID = c.Int(),
                    })
                .PrimaryKey(t => t.BaivietID)
                .ForeignKey("dbo.DanhMucs", t => t.DanhMucID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.BaiViets", t => t.BaiViet_BaivietID)
                .Index(t => t.DanhMucID)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.BaiViet_BaivietID);
            
            CreateTable(
                "dbo.DanhMucs",
                c => new
                    {
                        DanhmucID = c.Int(nullable: false, identity: true),
                        Tendanhmuc = c.String(),
                    })
                .PrimaryKey(t => t.DanhmucID);
            
            CreateTable(
                "dbo.Congcus",
                c => new
                    {
                        CongcuID = c.Int(nullable: false, identity: true),
                        TenCongCu = c.String(),
                    })
                .PrimaryKey(t => t.CongcuID);
            
            CreateTable(
                "dbo.Thuongs",
                c => new
                    {
                        Sodiemthuong = c.Int(nullable: false, identity: true),
                        Phanthuong = c.String(),
                    })
                .PrimaryKey(t => t.Sodiemthuong);
            
            CreateTable(
                "dbo.WebLienKets",
                c => new
                    {
                        WeblienketID = c.Int(nullable: false, identity: true),
                        Tenweb = c.String(),
                        Link = c.String(),
                    })
                .PrimaryKey(t => t.WeblienketID);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        BinhluanID = c.Int(nullable: false, identity: true),
                        Tieude = c.String(),
                        Noidung = c.String(),
                        Ngaygioviet = c.DateTime(nullable: false),
                        BaivietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BinhluanID)
                .ForeignKey("dbo.BaiViets", t => t.BaivietID, cascadeDelete: true)
                .Index(t => t.BaivietID);
            
            CreateTable(
                "dbo.DanhMuc_CongCu",
                c => new
                    {
                        DanhmucID = c.Int(nullable: false),
                        CongcuID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DanhmucID, t.CongcuID })
                .ForeignKey("dbo.DanhMucs", t => t.DanhmucID, cascadeDelete: true)
                .ForeignKey("dbo.Congcus", t => t.CongcuID, cascadeDelete: true)
                .Index(t => t.DanhmucID)
                .Index(t => t.CongcuID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.DanhMuc_CongCu", new[] { "CongcuID" });
            DropIndex("dbo.DanhMuc_CongCu", new[] { "DanhmucID" });
            DropIndex("dbo.BinhLuans", new[] { "BaivietID" });
            DropIndex("dbo.BaiViets", new[] { "BaiViet_BaivietID" });
            DropIndex("dbo.BaiViets", new[] { "UserProfile_UserId" });
            DropIndex("dbo.BaiViets", new[] { "DanhMucID" });
            DropForeignKey("dbo.DanhMuc_CongCu", "CongcuID", "dbo.Congcus");
            DropForeignKey("dbo.DanhMuc_CongCu", "DanhmucID", "dbo.DanhMucs");
            DropForeignKey("dbo.BinhLuans", "BaivietID", "dbo.BaiViets");
            DropForeignKey("dbo.BaiViets", "BaiViet_BaivietID", "dbo.BaiViets");
            DropForeignKey("dbo.BaiViets", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.BaiViets", "DanhMucID", "dbo.DanhMucs");
            DropTable("dbo.DanhMuc_CongCu");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.WebLienKets");
            DropTable("dbo.Thuongs");
            DropTable("dbo.Congcus");
            DropTable("dbo.DanhMucs");
            DropTable("dbo.BaiViets");
            DropTable("dbo.UserProfile");
        }
    }
}
