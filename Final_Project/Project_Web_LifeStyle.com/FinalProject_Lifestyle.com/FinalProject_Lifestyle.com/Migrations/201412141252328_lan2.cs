namespace FinalProject_Lifestyle.com.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BaiViets", "DanhMucID", "dbo.DanhMucs");
            DropForeignKey("dbo.BinhLuans", "BaivietID", "dbo.BaiViets");
            DropForeignKey("dbo.DanhMuc_CongCu", "DanhmucID", "dbo.DanhMucs");
            DropForeignKey("dbo.DanhMuc_CongCu", "CongcuID", "dbo.Congcus");
            DropIndex("dbo.BaiViets", new[] { "DanhMucID" });
            DropIndex("dbo.BinhLuans", new[] { "BaivietID" });
            DropIndex("dbo.DanhMuc_CongCu", new[] { "DanhmucID" });
            DropIndex("dbo.DanhMuc_CongCu", new[] { "CongcuID" });
            CreateTable(
                "dbo.TDTTs",
                c => new
                    {
                        TDTTID = c.Int(nullable: false, identity: true),
                        Tenbaiviet = c.String(),
                        Ngaygio = c.DateTime(nullable: false),
                        Tomtat = c.String(),
                        Noidung = c.String(),
                        Sodiemdanhgia = c.Int(nullable: false),
                        Solanxem = c.Int(nullable: false),
                        Hinhanh = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.TDTTID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.TangCans",
                c => new
                    {
                        BaivietID = c.Int(nullable: false, identity: true),
                        Tenbaiviet = c.String(),
                        Ngaygio = c.DateTime(nullable: false),
                        Tomtat = c.String(),
                        Noidung = c.String(),
                        Sodiemdanhgia = c.Int(nullable: false),
                        Solanxem = c.Int(nullable: false),
                        Hinhanh = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.BaivietID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.GiamCans",
                c => new
                    {
                        BaivietID = c.Int(nullable: false, identity: true),
                        Tenbaiviet = c.String(),
                        Ngaygio = c.DateTime(nullable: false),
                        Tomtat = c.String(),
                        Noidung = c.String(),
                        Sodiemdanhgia = c.Int(nullable: false),
                        Solanxem = c.Int(nullable: false),
                        Hinhanh = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.BaivietID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            AddColumn("dbo.BinhLuans", "BaivietID_TDTT", c => c.Int(nullable: false));
            AddColumn("dbo.BinhLuans", "BaivietID_TangCan", c => c.Int(nullable: false));
            AddColumn("dbo.BinhLuans", "BaiVietID_GiamCan", c => c.Int(nullable: false));
            AddColumn("dbo.BinhLuans", "TDTT_TDTTID", c => c.Int());
            AddColumn("dbo.BinhLuans", "TangCan_BaivietID", c => c.Int());
            AddColumn("dbo.BinhLuans", "GiamCan_BaivietID", c => c.Int());
            AddForeignKey("dbo.BinhLuans", "TDTT_TDTTID", "dbo.TDTTs", "TDTTID");
            AddForeignKey("dbo.BinhLuans", "TangCan_BaivietID", "dbo.TangCans", "BaivietID");
            AddForeignKey("dbo.BinhLuans", "GiamCan_BaivietID", "dbo.GiamCans", "BaivietID");
            CreateIndex("dbo.BinhLuans", "TDTT_TDTTID");
            CreateIndex("dbo.BinhLuans", "TangCan_BaivietID");
            CreateIndex("dbo.BinhLuans", "GiamCan_BaivietID");
            DropColumn("dbo.BaiViets", "DanhMucID");
            DropColumn("dbo.BinhLuans", "BaivietID");
            DropTable("dbo.DanhMucs");
            DropTable("dbo.DanhMuc_CongCu");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DanhMuc_CongCu",
                c => new
                    {
                        DanhmucID = c.Int(nullable: false),
                        CongcuID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DanhmucID, t.CongcuID });
            
            CreateTable(
                "dbo.DanhMucs",
                c => new
                    {
                        DanhmucID = c.Int(nullable: false, identity: true),
                        Tendanhmuc = c.String(),
                    })
                .PrimaryKey(t => t.DanhmucID);
            
            AddColumn("dbo.BinhLuans", "BaivietID", c => c.Int(nullable: false));
            AddColumn("dbo.BaiViets", "DanhMucID", c => c.Int(nullable: false));
            DropIndex("dbo.GiamCans", new[] { "UserProfile_UserId" });
            DropIndex("dbo.TangCans", new[] { "UserProfile_UserId" });
            DropIndex("dbo.BinhLuans", new[] { "GiamCan_BaivietID" });
            DropIndex("dbo.BinhLuans", new[] { "TangCan_BaivietID" });
            DropIndex("dbo.BinhLuans", new[] { "TDTT_TDTTID" });
            DropIndex("dbo.TDTTs", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.GiamCans", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.TangCans", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.BinhLuans", "GiamCan_BaivietID", "dbo.GiamCans");
            DropForeignKey("dbo.BinhLuans", "TangCan_BaivietID", "dbo.TangCans");
            DropForeignKey("dbo.BinhLuans", "TDTT_TDTTID", "dbo.TDTTs");
            DropForeignKey("dbo.TDTTs", "UserProfile_UserId", "dbo.UserProfile");
            DropColumn("dbo.BinhLuans", "GiamCan_BaivietID");
            DropColumn("dbo.BinhLuans", "TangCan_BaivietID");
            DropColumn("dbo.BinhLuans", "TDTT_TDTTID");
            DropColumn("dbo.BinhLuans", "BaiVietID_GiamCan");
            DropColumn("dbo.BinhLuans", "BaivietID_TangCan");
            DropColumn("dbo.BinhLuans", "BaivietID_TDTT");
            DropTable("dbo.GiamCans");
            DropTable("dbo.TangCans");
            DropTable("dbo.TDTTs");
            CreateIndex("dbo.DanhMuc_CongCu", "CongcuID");
            CreateIndex("dbo.DanhMuc_CongCu", "DanhmucID");
            CreateIndex("dbo.BinhLuans", "BaivietID");
            CreateIndex("dbo.BaiViets", "DanhMucID");
            AddForeignKey("dbo.DanhMuc_CongCu", "CongcuID", "dbo.Congcus", "CongcuID", cascadeDelete: true);
            AddForeignKey("dbo.DanhMuc_CongCu", "DanhmucID", "dbo.DanhMucs", "DanhmucID", cascadeDelete: true);
            AddForeignKey("dbo.BinhLuans", "BaivietID", "dbo.BaiViets", "BaivietID", cascadeDelete: true);
            AddForeignKey("dbo.BaiViets", "DanhMucID", "dbo.DanhMucs", "DanhmucID", cascadeDelete: true);
        }
    }
}
