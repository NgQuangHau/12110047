// <auto-generated />
namespace FinalProject_Lifestyle.com.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan3));
        
        string IMigrationMetadata.Id
        {
            get { return "201412140517201_lan3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
