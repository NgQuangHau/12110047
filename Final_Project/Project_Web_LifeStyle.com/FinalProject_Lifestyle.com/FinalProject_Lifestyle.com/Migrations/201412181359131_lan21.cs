namespace FinalProject_Lifestyle.com.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan21 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.String(nullable: false, maxLength: 128),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_GiamCan",
                c => new
                    {
                        GiamCanID = c.Int(nullable: false),
                        TagID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.GiamCanID, t.TagID })
                .ForeignKey("dbo.GiamCans", t => t.GiamCanID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.GiamCanID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_TangCan",
                c => new
                    {
                        TangCanID = c.Int(nullable: false),
                        TagID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.TangCanID, t.TagID })
                .ForeignKey("dbo.TangCans", t => t.TangCanID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.TangCanID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_TDTT",
                c => new
                    {
                        TDTTID = c.Int(nullable: false),
                        TagID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.TDTTID, t.TagID })
                .ForeignKey("dbo.TDTTs", t => t.TDTTID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.TDTTID)
                .Index(t => t.TagID);
            
            AlterColumn("dbo.TDTTs", "Tenbaiviet", c => c.String(maxLength: 100));
            AlterColumn("dbo.TDTTs", "Tomtat", c => c.String(maxLength: 100));
            AlterColumn("dbo.TDTTs", "Noidung", c => c.String(maxLength: 2000));
            AlterColumn("dbo.TangCans", "Tenbaiviet", c => c.String(maxLength: 100));
            AlterColumn("dbo.TangCans", "Tomtat", c => c.String(maxLength: 100));
            AlterColumn("dbo.TangCans", "Noidung", c => c.String(maxLength: 2000));
            AlterColumn("dbo.GiamCans", "Tenbaiviet", c => c.String(maxLength: 100));
            AlterColumn("dbo.GiamCans", "Tomtat", c => c.String(maxLength: 100));
            AlterColumn("dbo.GiamCans", "Noidung", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_TDTT", new[] { "TagID" });
            DropIndex("dbo.Tag_TDTT", new[] { "TDTTID" });
            DropIndex("dbo.Tag_TangCan", new[] { "TagID" });
            DropIndex("dbo.Tag_TangCan", new[] { "TangCanID" });
            DropIndex("dbo.Tag_GiamCan", new[] { "TagID" });
            DropIndex("dbo.Tag_GiamCan", new[] { "GiamCanID" });
            DropForeignKey("dbo.Tag_TDTT", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_TDTT", "TDTTID", "dbo.TDTTs");
            DropForeignKey("dbo.Tag_TangCan", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_TangCan", "TangCanID", "dbo.TangCans");
            DropForeignKey("dbo.Tag_GiamCan", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_GiamCan", "GiamCanID", "dbo.GiamCans");
            AlterColumn("dbo.GiamCans", "Noidung", c => c.String());
            AlterColumn("dbo.GiamCans", "Tomtat", c => c.String());
            AlterColumn("dbo.GiamCans", "Tenbaiviet", c => c.String());
            AlterColumn("dbo.TangCans", "Noidung", c => c.String());
            AlterColumn("dbo.TangCans", "Tomtat", c => c.String());
            AlterColumn("dbo.TangCans", "Tenbaiviet", c => c.String());
            AlterColumn("dbo.TDTTs", "Noidung", c => c.String());
            AlterColumn("dbo.TDTTs", "Tomtat", c => c.String());
            AlterColumn("dbo.TDTTs", "Tenbaiviet", c => c.String());
            DropTable("dbo.Tag_TDTT");
            DropTable("dbo.Tag_TangCan");
            DropTable("dbo.Tag_GiamCan");
            DropTable("dbo.Tags");
        }
    }
}
