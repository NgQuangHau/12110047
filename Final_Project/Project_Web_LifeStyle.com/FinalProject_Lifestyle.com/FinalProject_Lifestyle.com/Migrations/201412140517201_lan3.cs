namespace FinalProject_Lifestyle.com.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BaiViets", "BaiViet_BaivietID", "dbo.BaiViets");
            DropIndex("dbo.BaiViets", new[] { "BaiViet_BaivietID" });
            DropColumn("dbo.BaiViets", "BaiViet_BaivietID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BaiViets", "BaiViet_BaivietID", c => c.Int());
            CreateIndex("dbo.BaiViets", "BaiViet_BaivietID");
            AddForeignKey("dbo.BaiViets", "BaiViet_BaivietID", "dbo.BaiViets", "BaivietID");
        }
    }
}
