﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class ThuongController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /Thuong/

        public ActionResult Index()
        {
            return View(db.Thuongs.ToList());
        }

        //
        // GET: /Thuong/Details/5

        public ActionResult Details(int id = 0)
        {
            Thuong thuong = db.Thuongs.Find(id);
            if (thuong == null)
            {
                return HttpNotFound();
            }
            return View(thuong);
        }

        //
        // GET: /Thuong/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Thuong/Create

        [HttpPost]
        public ActionResult Create(Thuong thuong)
        {
            if (ModelState.IsValid)
            {
                db.Thuongs.Add(thuong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thuong);
        }

        //
        // GET: /Thuong/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Thuong thuong = db.Thuongs.Find(id);
            if (thuong == null)
            {
                return HttpNotFound();
            }
            return View(thuong);
        }

        //
        // POST: /Thuong/Edit/5

        [HttpPost]
        public ActionResult Edit(Thuong thuong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thuong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thuong);
        }

        //
        // GET: /Thuong/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Thuong thuong = db.Thuongs.Find(id);
            if (thuong == null)
            {
                return HttpNotFound();
            }
            return View(thuong);
        }

        //
        // POST: /Thuong/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Thuong thuong = db.Thuongs.Find(id);
            db.Thuongs.Remove(thuong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}