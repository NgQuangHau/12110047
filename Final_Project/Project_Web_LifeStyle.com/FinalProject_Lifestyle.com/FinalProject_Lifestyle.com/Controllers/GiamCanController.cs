﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class GiamCanController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /GiamCan/

        public ActionResult Index()
        {
            return View(db.GiamCans.ToList());
        }

        //
        // GET: /GiamCan/Details/5

        public ActionResult Details(int id = 0)
        {
            GiamCan giamcan = db.GiamCans.Find(id);
            if (giamcan == null)
            {
                return HttpNotFound();
            }
            return View(giamcan);
        }

        //
        // GET: /GiamCan/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /GiamCan/Create

        [HttpPost]
        public ActionResult Create(GiamCan giamcan)
        {
            if (ModelState.IsValid)
            {
                db.GiamCans.Add(giamcan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(giamcan);
        }

        //
        // GET: /GiamCan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            GiamCan giamcan = db.GiamCans.Find(id);
            if (giamcan == null)
            {
                return HttpNotFound();
            }
            return View(giamcan);
        }

        //
        // POST: /GiamCan/Edit/5

        [HttpPost]
        public ActionResult Edit(GiamCan giamcan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(giamcan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(giamcan);
        }

        //
        // GET: /GiamCan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            GiamCan giamcan = db.GiamCans.Find(id);
            if (giamcan == null)
            {
                return HttpNotFound();
            }
            return View(giamcan);
        }

        //
        // POST: /GiamCan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            GiamCan giamcan = db.GiamCans.Find(id);
            db.GiamCans.Remove(giamcan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}