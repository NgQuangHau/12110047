﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class CongCuController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /CongCu/

        public ActionResult Index()
        {
            return View(db.Congcus.ToList());
        }

        //
        // GET: /CongCu/Details/5

        public ActionResult Details(int id = 0)
        {
            Congcu congcu = db.Congcus.Find(id);
            if (congcu == null)
            {
                return HttpNotFound();
            }
            return View(congcu);
        }

        //
        // GET: /CongCu/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CongCu/Create

        [HttpPost]
        public ActionResult Create(Congcu congcu)
        {
            if (ModelState.IsValid)
            {
                db.Congcus.Add(congcu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(congcu);
        }

        //
        // GET: /CongCu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Congcu congcu = db.Congcus.Find(id);
            if (congcu == null)
            {
                return HttpNotFound();
            }
            return View(congcu);
        }

        //
        // POST: /CongCu/Edit/5

        [HttpPost]
        public ActionResult Edit(Congcu congcu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(congcu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(congcu);
        }

        //
        // GET: /CongCu/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Congcu congcu = db.Congcus.Find(id);
            if (congcu == null)
            {
                return HttpNotFound();
            }
            return View(congcu);
        }

        //
        // POST: /CongCu/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Congcu congcu = db.Congcus.Find(id);
            db.Congcus.Remove(congcu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}