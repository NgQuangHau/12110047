﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class TDTTController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /TDTT/

        public ActionResult Index()
        {
            return View(db.TDTTs.ToList());
        }

        //
        // GET: /TDTT/Details/5

        public ActionResult Details(int id = 0)
        {
            TDTT tdtt = db.TDTTs.Find(id);
            
            if (tdtt == null)
            {
                return HttpNotFound();
            }
            ViewData["idTDTT"] = id;
            return View(tdtt);
        }

        //
        // GET: /TDTT/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TDTT/Create

        [HttpPost]
        public ActionResult Create(TDTT tdtt)
        {
            if (ModelState.IsValid)
            {
                db.TDTTs.Add(tdtt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tdtt);
        }

        //
        // GET: /TDTT/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TDTT tdtt = db.TDTTs.Find(id);
            if (tdtt == null)
            {
                return HttpNotFound();
            }
            return View(tdtt);
        }

        //
        // POST: /TDTT/Edit/5

        [HttpPost]
        public ActionResult Edit(TDTT tdtt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tdtt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tdtt);
        }

        //
        // GET: /TDTT/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TDTT tdtt = db.TDTTs.Find(id);
            if (tdtt == null)
            {
                return HttpNotFound();
            }
            return View(tdtt);
        }

        //
        // POST: /TDTT/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TDTT tdtt = db.TDTTs.Find(id);
            db.TDTTs.Remove(tdtt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}