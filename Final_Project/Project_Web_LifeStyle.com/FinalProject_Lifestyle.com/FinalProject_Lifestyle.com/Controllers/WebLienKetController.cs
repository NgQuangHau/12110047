﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class WebLienKetController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /WebLienKet/

        public ActionResult Index()
        {
            return View(db.WebLienKets.ToList());
        }

        //
        // GET: /WebLienKet/Details/5

        public ActionResult Details(int id = 0)
        {
            WebLienKet weblienket = db.WebLienKets.Find(id);
            if (weblienket == null)
            {
                return HttpNotFound();
            }
            return View(weblienket);
        }

        //
        // GET: /WebLienKet/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /WebLienKet/Create

        [HttpPost]
        public ActionResult Create(WebLienKet weblienket)
        {
            if (ModelState.IsValid)
            {
                db.WebLienKets.Add(weblienket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(weblienket);
        }

        //
        // GET: /WebLienKet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            WebLienKet weblienket = db.WebLienKets.Find(id);
            if (weblienket == null)
            {
                return HttpNotFound();
            }
            return View(weblienket);
        }

        //
        // POST: /WebLienKet/Edit/5

        [HttpPost]
        public ActionResult Edit(WebLienKet weblienket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weblienket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(weblienket);
        }

        //
        // GET: /WebLienKet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            WebLienKet weblienket = db.WebLienKets.Find(id);
            if (weblienket == null)
            {
                return HttpNotFound();
            }
            return View(weblienket);
        }

        //
        // POST: /WebLienKet/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            WebLienKet weblienket = db.WebLienKets.Find(id);
            db.WebLienKets.Remove(weblienket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}