﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class BinhLuanController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /BinhLuan/

        public ActionResult Index()
        {
            return View(db.BinhLuans.ToList());
        }

        //
        // GET: /BinhLuan/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Create

        public ActionResult Create()
        {
            ViewBag.TDTTID = new SelectList(db.TDTTs, "BaivietID_TDTT", "Tenbaiviet");
            return View();
        }

        //
        // POST: /BinhLuan/Create

        [HttpPost]
        public ActionResult Create(BinhLuan binhluan, int idTDTT)
        {
            if (ModelState.IsValid)
            {
                binhluan.BaivietID_TDTT = idTDTT;
                binhluan.Ngaygioviet = DateTime.Now;
                db.BinhLuans.Add(binhluan);
                db.SaveChanges();
                return RedirectToAction("Details/"+idTDTT,"TDTT");
            }

            ViewBag.BaivietID_TDTT = new SelectList(db.TDTTs, "BaivietID_TDTT", "Tenbaiviet", binhluan.BaivietID_TDTT);

            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Edit/5

        [HttpPost]
        public ActionResult Edit(BinhLuan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            db.BinhLuans.Remove(binhluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}