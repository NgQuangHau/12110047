﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject_Lifestyle.com.Models;

namespace FinalProject_Lifestyle.com.Controllers
{
    public class TangCanController : Controller
    {
        private LifestyleDbContext db = new LifestyleDbContext();

        //
        // GET: /TangCan/

        public ActionResult Index()
        {
            return View(db.TangCans.ToList());
        }

        //
        // GET: /TangCan/Details/5

        public ActionResult Details(int id = 0)
        {
            TangCan tangcan = db.TangCans.Find(id);
            if (tangcan == null)
            {
                return HttpNotFound();
            }
            return View(tangcan);
        }

        //
        // GET: /TangCan/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TangCan/Create

        [HttpPost]
        public ActionResult Create(TangCan tangcan)
        {
            if (ModelState.IsValid)
            {
                db.TangCans.Add(tangcan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tangcan);
        }

        //
        // GET: /TangCan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TangCan tangcan = db.TangCans.Find(id);
            if (tangcan == null)
            {
                return HttpNotFound();
            }
            return View(tangcan);
        }

        //
        // POST: /TangCan/Edit/5

        [HttpPost]
        public ActionResult Edit(TangCan tangcan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tangcan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tangcan);
        }

        //
        // GET: /TangCan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TangCan tangcan = db.TangCans.Find(id);
            if (tangcan == null)
            {
                return HttpNotFound();
            }
            return View(tangcan);
        }

        //
        // POST: /TangCan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TangCan tangcan = db.TangCans.Find(id);
            db.TangCans.Remove(tangcan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}