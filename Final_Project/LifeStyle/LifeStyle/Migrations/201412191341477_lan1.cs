namespace LifeStyle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.TDTTs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        Body = c.String(maxLength: 2000),
                        DayCreate = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        Body = c.String(maxLength: 2000),
                        DayCreate = c.DateTime(nullable: false),
                        Author = c.String(),
                        TDTTID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TDTTs", t => t.TDTTID, cascadeDelete: true)
                .Index(t => t.TDTTID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.BinhLuans", new[] { "TDTTID" });
            DropIndex("dbo.TDTTs", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.BinhLuans", "TDTTID", "dbo.TDTTs");
            DropForeignKey("dbo.TDTTs", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.TDTTs");
            DropTable("dbo.UserProfile");
        }
    }
}
