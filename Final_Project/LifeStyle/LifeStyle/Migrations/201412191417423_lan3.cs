namespace LifeStyle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BinhLuans", "TangCan_TangCanID", "dbo.TangCans");
            DropIndex("dbo.BinhLuans", new[] { "TangCan_TangCanID" });
            CreateTable(
                "dbo.BinhLuan_TangCan",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        Body = c.String(maxLength: 2000),
                        DayCreate = c.DateTime(nullable: false),
                        Author = c.String(),
                        TangCan_ID = c.Int(nullable: false),
                        TangCan_TangCanID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TangCans", t => t.TangCan_TangCanID)
                .Index(t => t.TangCan_TangCanID);
            
            DropColumn("dbo.BinhLuans", "TangCan_ID");
            DropColumn("dbo.BinhLuans", "TangCan_TangCanID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BinhLuans", "TangCan_TangCanID", c => c.Int());
            AddColumn("dbo.BinhLuans", "TangCan_ID", c => c.Int(nullable: false));
            DropIndex("dbo.BinhLuan_TangCan", new[] { "TangCan_TangCanID" });
            DropForeignKey("dbo.BinhLuan_TangCan", "TangCan_TangCanID", "dbo.TangCans");
            DropTable("dbo.BinhLuan_TangCan");
            CreateIndex("dbo.BinhLuans", "TangCan_TangCanID");
            AddForeignKey("dbo.BinhLuans", "TangCan_TangCanID", "dbo.TangCans", "TangCanID");
        }
    }
}
