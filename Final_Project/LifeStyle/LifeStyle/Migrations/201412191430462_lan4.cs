namespace LifeStyle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GiamCans",
                c => new
                    {
                        GiamCanID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        Body = c.String(maxLength: 2000),
                        DayCreate = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.GiamCanID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.CongCus",
                c => new
                    {
                        CongCuID = c.Int(nullable: false, identity: true),
                        TenCongCu = c.String(),
                    })
                .PrimaryKey(t => t.CongCuID);
            
            CreateTable(
                "dbo.WebLienKets",
                c => new
                    {
                        WeblienketID = c.Int(nullable: false, identity: true),
                        TenWebLienKet = c.String(),
                    })
                .PrimaryKey(t => t.WeblienketID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.GiamCans", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.GiamCans", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.WebLienKets");
            DropTable("dbo.CongCus");
            DropTable("dbo.GiamCans");
        }
    }
}
