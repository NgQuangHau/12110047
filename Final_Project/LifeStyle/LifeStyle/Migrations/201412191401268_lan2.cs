namespace LifeStyle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TangCans",
                c => new
                    {
                        TangCanID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        Body = c.String(maxLength: 2000),
                        DayCreate = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.TangCanID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            AddColumn("dbo.BinhLuans", "TangCan_ID", c => c.Int(nullable: false));
            AddColumn("dbo.BinhLuans", "TangCan_TangCanID", c => c.Int());
            AddForeignKey("dbo.BinhLuans", "TangCan_TangCanID", "dbo.TangCans", "TangCanID");
            CreateIndex("dbo.BinhLuans", "TangCan_TangCanID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TangCans", new[] { "UserProfile_UserId" });
            DropIndex("dbo.BinhLuans", new[] { "TangCan_TangCanID" });
            DropForeignKey("dbo.TangCans", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.BinhLuans", "TangCan_TangCanID", "dbo.TangCans");
            DropColumn("dbo.BinhLuans", "TangCan_TangCanID");
            DropColumn("dbo.BinhLuans", "TangCan_ID");
            DropTable("dbo.TangCans");
        }
    }
}
