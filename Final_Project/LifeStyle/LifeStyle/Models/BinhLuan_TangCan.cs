﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LifeStyle.Models
{
    public class BinhLuan_TangCan
    {
        public int Id { set; get; }
        [StringLength(50, ErrorMessage = "Độ dài từ 10 đến 50 kí tự", MinimumLength = 10)]
        public String Title { set; get; }
        [StringLength(2000, ErrorMessage = "Độ dài từ 5 đến 2000 kí tự", MinimumLength = 5)]
        public String Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DayCreate { set; get; }

        public int Lasttime
        {
            get
            {
                return (DateTime.Now - DayCreate).Minutes;
            }
        }

        public String Author { set; get; }

        public int TangCan_ID { set; get; }
        public virtual TangCan TangCan { set; get; }
    }
    
}