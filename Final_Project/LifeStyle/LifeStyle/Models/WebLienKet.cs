﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LifeStyle.Models
{
    public class WebLienKet
    {
        public int WeblienketID { set; get; }
        public String TenWebLienKet { set; get; }
    }
}