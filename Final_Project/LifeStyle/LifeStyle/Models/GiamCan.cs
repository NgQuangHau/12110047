﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LifeStyle.Models
{
    public class GiamCan
    {
        public int GiamCanID { set; get; }
        [StringLength(50, ErrorMessage = "Độ dài từ 10 đến 50 kí tự", MinimumLength = 10)]
        public String Title { set; get; }
        [StringLength(2000, ErrorMessage = "Độ dài từ 5 đến 500 kí tự", MinimumLength = 5)]
        public String Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DayCreate { set; get; }

        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }
}