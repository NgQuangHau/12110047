﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LifeStyle.Models;

namespace LifeStyle.Controllers
{
    public class CongCuController : Controller
    {
        private LifeStyleDBContext db = new LifeStyleDBContext();

        //
        // GET: /CongCu/

        public ActionResult Index()
        {
            return View(db.CongCus.ToList());
        }

        //
        // GET: /CongCu/Details/5

        public ActionResult Details(int id = 0)
        {
            CongCu congcu = db.CongCus.Find(id);
            if (congcu == null)
            {
                return HttpNotFound();
            }
            return View(congcu);
        }

        //
        // GET: /CongCu/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CongCu/Create

        [HttpPost]
        public ActionResult Create(CongCu congcu)
        {
            if (ModelState.IsValid)
            {
                db.CongCus.Add(congcu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(congcu);
        }

        //
        // GET: /CongCu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CongCu congcu = db.CongCus.Find(id);
            if (congcu == null)
            {
                return HttpNotFound();
            }
            return View(congcu);
        }

        //
        // POST: /CongCu/Edit/5

        [HttpPost]
        public ActionResult Edit(CongCu congcu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(congcu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(congcu);
        }

        //
        // GET: /CongCu/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CongCu congcu = db.CongCus.Find(id);
            if (congcu == null)
            {
                return HttpNotFound();
            }
            return View(congcu);
        }

        //
        // POST: /CongCu/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CongCu congcu = db.CongCus.Find(id);
            db.CongCus.Remove(congcu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}