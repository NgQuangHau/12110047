﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LifeStyle.Models;

namespace LifeStyle.Controllers
{
    public class BinhLuan_TangCanController : Controller
    {
        private LifeStyleDBContext db = new LifeStyleDBContext();

        //
        // GET: /BinhLuan_TangCan/

        public ActionResult Index()
        {
            return View(db.BinhLuan_TangCans.ToList());
        }

        //
        // GET: /BinhLuan_TangCan/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuan_TangCan binhluan_tangcan = db.BinhLuan_TangCans.Find(id);
            if (binhluan_tangcan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan_tangcan);
        }

        //
        // GET: /BinhLuan_TangCan/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BinhLuan_TangCan/Create

        [HttpPost]
        public ActionResult Create(BinhLuan_TangCan binhluan_tangcan)
        {
            if (ModelState.IsValid)
            {
                db.BinhLuan_TangCans.Add(binhluan_tangcan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(binhluan_tangcan);
        }

        //
        // GET: /BinhLuan_TangCan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuan_TangCan binhluan_tangcan = db.BinhLuan_TangCans.Find(id);
            if (binhluan_tangcan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan_tangcan);
        }

        //
        // POST: /BinhLuan_TangCan/Edit/5

        [HttpPost]
        public ActionResult Edit(BinhLuan_TangCan binhluan_tangcan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan_tangcan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(binhluan_tangcan);
        }

        //
        // GET: /BinhLuan_TangCan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuan_TangCan binhluan_tangcan = db.BinhLuan_TangCans.Find(id);
            if (binhluan_tangcan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan_tangcan);
        }

        //
        // POST: /BinhLuan_TangCan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuan_TangCan binhluan_tangcan = db.BinhLuan_TangCans.Find(id);
            db.BinhLuan_TangCans.Remove(binhluan_tangcan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}