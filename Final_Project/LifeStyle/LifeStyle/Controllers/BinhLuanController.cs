﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LifeStyle.Models;

namespace LifeStyle.Controllers
{
    public class BinhLuanController : Controller
    {
        private LifeStyleDBContext db = new LifeStyleDBContext();

        //
        // GET: /BinhLuan/

        public ActionResult Index()
        {
            var binhluans = db.BinhLuans.Include(b => b.TDTT);
            return View(binhluans.ToList());
        }

        //
        // GET: /BinhLuan/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Create

        public ActionResult Create()
        {
            ViewBag.TDTTID = new SelectList(db.TDTTs, "ID", "Title");
            return View();
        }

        //
        // POST: /BinhLuan/Create

        [HttpPost]
        public ActionResult Create(BinhLuan binhluan, int idtdtt)
        {
            if (ModelState.IsValid)
            {
                binhluan.TDTTID = idtdtt;
                binhluan.DayCreate = DateTime.Now;
                db.BinhLuans.Add(binhluan);
                db.SaveChanges();
                return RedirectToAction("Details/" + idtdtt, "TDTT");
            }

            ViewBag.TDTTID = new SelectList(db.TDTTs, "ID", "Title", binhluan.TDTTID);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            ViewBag.TDTTID = new SelectList(db.TDTTs, "ID", "Title", binhluan.TDTTID);
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Edit/5

        [HttpPost]
        public ActionResult Edit(BinhLuan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TDTTID = new SelectList(db.TDTTs, "ID", "Title", binhluan.TDTTID);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            db.BinhLuans.Remove(binhluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}