﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        [Required]

        [StringLength(100, ErrorMessage = "Độ dài từ 10 đến 100 kí tự", MinimumLength = 10)]
        public String Content { get; set; }


        public virtual ICollection<Post> Posts { get; set; }
    }
}