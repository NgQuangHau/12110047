﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_.Models
{
    public class Comment
    {
        public int ID { get; set; }
        [Required]

        [StringLength(60000, ErrorMessage = "Độ dài phải từ 50 kí tự", MinimumLength = 50)]
        public String Body { get; set; }

        [Required]
        public String Author { get; set; }


        [DataType(DataType.Date, ErrorMessage = "Định dạng kiểu datetime")]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.Date, ErrorMessage = "Định dạng kiểu datetime")]
        public DateTime DateUpdated { get; set; }



        public int PostID { get; set; }
        public virtual Post post { get; set; }
    }
}