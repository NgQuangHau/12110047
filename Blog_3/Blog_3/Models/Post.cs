﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_.Models
{
    public class Post
    {

        public int ID { get; set; }
        [Required]


        [StringLength(500, ErrorMessage = "Độ dài từ 10 đến 500 kí tự", MinimumLength = 20)]
        public String Title { get; set; }

        [StringLength(60000, ErrorMessage = "Độ dài phải từ 50 kí tự", MinimumLength = 50)]
        public String Body { get; set; }



        [DataType(DataType.Date, ErrorMessage = "Định dạng kiểu datetime")]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.Date, ErrorMessage = "Định dạng kiểu datetime")]
        public DateTime DateUpdated { get; set; }


        public virtual ICollection<Comment> Comments { set; get; }

        public String AccountID { set; get; }
        public virtual Account account { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }
    }


}