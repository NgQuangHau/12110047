﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog1.Models
{
    public class Comments
    {
        [Key]
        public int Id { set; get; }
        public int Post_id { set; get; }
        public String Body { set; get; }
    }
}