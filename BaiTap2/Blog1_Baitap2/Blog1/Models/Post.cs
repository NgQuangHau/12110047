﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog1.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public String Title { set; get; }
        public String Body { set; get; }

    }
    public class BlogDbContext : DbContext
    {
        public DbSet<Post> Post { set; get; }
        public DbSet<Comments> Comments { set; get; }
    }
}