﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_T5.Models
{
    public class Comment
    {
        public int Id { set; get; }
         [StringLength(500, ErrorMessage = "Độ dài từ 20 đến 500 kí tự", MinimumLength = 20)]
        public String Title { set; get; }
        [StringLength(500, ErrorMessage = "Độ dài từ 50 đến 500 kí tự", MinimumLength = 50)]
        public String Body { set; get; }
    
        public DateTime DayCreate { set; get; }

        public int Lasttime
        {
            get
            {
                return (DateTime.Now - DayCreate).Minutes;
            }
        }

        public String Author { set; get; }

        public virtual Post Post { set; get; }
        public int PostID { set; get; }
    }
}