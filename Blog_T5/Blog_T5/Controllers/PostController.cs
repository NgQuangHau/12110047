﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_T5.Models;

namespace Blog_T5.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }

        public ActionResult ViewLinkTagByID(int ID)
        {
            return View(db.Tags.Single(x => x.ID == ID));
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [Authorize]
        [HttpPost]
        public ActionResult Create(Post post, String Content)
        {
            if (ModelState.IsValid)
            {
                post.DayCreate = DateTime.Now;
                //list cac tag
                List<Tag> Tags = new List<Tag>();
                //Tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem tag content da co hay chua
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        // neu co roi thi add them post
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //add vao list cac tag
                    Tags.Add(tagExits);
                }
                //Gán list tag cho post
                post.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileID = userid;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5
        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5
        [Authorize]
        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}